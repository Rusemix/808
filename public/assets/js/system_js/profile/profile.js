$(document).ready(function () {
    /* defined datas */
    var dataTargetProfit = []
    var dataProfit = [
        [1575842400000, 10],
        [1575928800000, 15],
        [1576015200000, 4],
        [1576101600000, 20],
        [1576188000000, 23],
        [1576274400000, 15],
        [1576360800000, 4],
        [1576447200000, 15],
        [1576533600000, 23],
        [1576620000000, 32],
        [1576706400000, 5],
        [1576792800000, 10]
    ]
    var dataSignups = [
        [1575842400000, 2],
        [1575928800000, 4],
        [1576015200000, 2],
        [1576101600000, 7],
        [1576188000000, 5],
        [1576274400000, 2],
        [1576360800000, 4],
        [1576447200000, 3],
        [1576533600000, 5],
        [1576620000000, 2],
        [1576706400000, 1],
        [1576792800000, 7]
    ]

    var data = [],
        totalPoints = 50;

    /* generate random data */
    var getRandomData = function () {
        if (data.length > 0)
            data = data.slice(1);
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            }
            else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }

    /* generate random data -- end */


    function liveStartProces() {

//            if ($('input[type="checkbox"]#start_interval').prop('checked'))
//            {
        $on = true;
        updateInterval = 10000;
        update();
//            }
//            else
//            {
//                clearInterval(updateInterval);
//                $on = true;
//            }
    }


    $('input[type="checkbox"]#start_interval').click(function () {
        liveStartProces();
    });

    // setTimeout(liveStartProces, 4300);
    setTimeout(liveStartProces, 4300);

    /* TAB 1: UPDATING CHART */
    var data = [],
        totalPoints = 200;

// var getRandomData = function()
// {
// //            if (data.length > 0)
// //                data = data.slice(1);
// //
// //            // do a random walk
// //            while (data.length < totalPoints)
// //            {
// //                var prev = data.length > 0 ? data[data.length - 1] : 50;
// //                var y = prev + Math.random() * 10 - 5;
// //                if (y < 0)
// //                    y = 0;
// //                if (y > 100)
// //                    y = 100;
// //                data.push(y);
// //            }
// //
// //            // zip the generated y values with the x values
//     var res = [];
// //            for (var i = 0; i < data.length; ++i)
//     res.push([0, 2])
//     res.push([1, 4])
//     res.push([2, 2])
//     return res;
// }
// setup control widget
    var updateInterval = 1500;
    $("#updating-chart").val(updateInterval).change(function () {

        var v = $(this).val();
        if (v && !isNaN(+v)) {
            updateInterval = +v;
            $(this).val("" + updateInterval);
        }

    });
// setup plot
    var options = {
        colors: [myapp_get_color.primary_700],
        series:
            {
                lines:
                    {
                        show: true,
                        lineWidth: 0.5,
                        fill: 0.9,
                        fillColor:
                            {
                                colors: [
                                    {
                                        opacity: 0.6
                                    },
                                    {
                                        opacity: 0
                                    }]
                            },
                    },

                shadowSize: 0 // Drawing is faster without shadows
            },
        grid:
            {
                borderColor: '#F0F0F0',
                borderWidth: 1,
                labelMargin: 5
            },
        xaxis:
            {
                color: '#F0F0F0',
                font:
                    {
                        size: 10,
                        color: '#999'
                    }
            },
        yaxis:
            {
                min: 0,
                max: 100,
                color: '#F0F0F0',
                font:
                    {
                        size: 10,
                        color: '#999'
                    }
            }
    };

    console.log(getRandomData());
    var plot = $.plot($("#updating-chart"), [getRandomData()], options);
    /* live switch */


    var update = function () {
        if ($on == true) {
            plot.setData([getRandomData()]);
            plot.draw();
            setTimeout(update, updateInterval);

        }
        else {
            clearInterval(updateInterval)
        }

    };


    /* flot toggle example */
    var flot_toggle = function () {

        var data = [
            {
                label: "Target Profit",
                data: dataTargetProfit,
                color: myapp_get_color.danger_500,
                bars:
                    {
                        show: false,
                        align: "center",
                        barWidth: 30 * 30 * 60 * 1000 * 80,
                        lineWidth: 0,
                        fillColor:
                            {
                                colors: [myapp_get_color.danger_900, myapp_get_color.danger_100]
                            }
                    },
                highlightColor: 'rgba(255,255,255,0.3)',
                shadowSize: 0
            },
            {
                label: "Success request",
                data: dataProfit,
                color: myapp_get_color.info_500,
                lines:
                    {
                        show: true,
                        lineWidth: 5
                    },
                shadowSize: 0,
                points:
                    {
                        show: true
                    }
            },
            {
                label: "Fail request",
                data: dataSignups,
                color: myapp_get_color.success_500,
                lines:
                    {
                        show: true,
                        lineWidth: 2
                    },
                shadowSize: 0,
                points:
                    {
                        show: true
                    }
            }
        ]

        var options = {
            grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
            tooltip: true,
            tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false
                },
            xaxis:
                {
                    mode: "time"
                },
            yaxes:
                {
                    tickFormatter: function (val, axis) {
                        return "$" + val;
                    },
                    max: 1200
                }

        };

        var plot2 = null;

        function plotNow() {
            var d = [];
            $("#js-checkbox-toggles").find(':checkbox').each(function () {
                if ($(this).is(':checked')) {
                    d.push(data[$(this).attr("name").substr(4, 1)]);
                }
            });
            if (d.length > 0) {
                if (plot2) {
                    plot2.setData(d);
                    plot2.draw();
                }
                else {
                    plot2 = $.plot($("#flot-toggles"), d, options);
                }
            }

        };

        $("#js-checkbox-toggles").find(':checkbox').on('change', function () {
            plotNow();
        });
        plotNow()
    }
    flot_toggle();
    /* flot toggle example -- end*/

});
