$(document).ready(function () {
    // setInterval(getNewOrder, 5000)
    getNewOrder()
    function getNewOrder() {
        $.ajax({
            url: '/getNewOrders',
            type: "POST",
            success: function (data) {
                $('.orders-menu span').html('Orders <span style="color: #ff4a4a;">(' + data.count + ')</span>');
            }
        });
    }
});