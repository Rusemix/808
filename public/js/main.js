$(document).ready(function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {

    } else {
        $('#menu-services li a').removeAttr('href')
    }

    var anc = window.location.hash.replace("#","");
    $('#menu-services li').each(function(i, v){
        if($(v).find('a').attr('href') == '#'+anc){
            $('#menu-services li a').removeClass('active');
            $(this).find('a').addClass('active');
            var servicesId = $(v).find('a').attr('services-id');
            $.ajax({
                url: '/apiGetServices',
                type: "POST",
                dataType: "json",
                data: {
                    "servicesId": servicesId
                },
                success: function (data) {
                    // $('.image-services').css('background-image', 'url(' + encodeURI(data.image) + ')');
                    $('.image-services-url').attr('src', encodeURI(data.image));
                    if (!data.description) {
                        $('.text-services').html("");
                        $('.price').html("");
                    } else {
                        $('.price').text('$' + data.price);
                        $('.text-services').html(data.description);
                    }
                }
            });
        }
    });
    // $('.icons-social').hover(function () {
    //         $(this).parent('a').find('.icons-social').css('display', 'none');
    //         $(this).parent('a').find('.icons-social-hover').css('display', 'inline');
    //     },
    //     function () {
    //         $(this).parent('a').find('.icons-social').css('display', 'inline');
    //         $(this).parent('a').find('.icons-social-hover').css('display', 'none');
    //     })

    $('#block-request .request').hover(function () {
            $('#block-request .request').css('height', '198px');
            $('#block-request .request-message').css('margin', '15px 0px 0px -14px');
            $('#block-request .request-facebook').css('display', 'inline');
            $('#block-request .request-yelp').css('display', 'inline');
        },
        function () {
            $('#block-request .request').css('height', '60px');
            $('#block-request .request-message').css('margin', '-4px 0px 0px -14px');
            $('#block-request .request-facebook').css('display', 'none');
            $('#block-request .request-yelp').css('display', 'none');
        })
    $(document).on('keyup', '.message-order', function () {
        $(this).parents('.form-group').find('.length-text').text($(this).val().length + '/500');
        $(this).val($(this).val().substr(0, 500));
        if ($(this).val().length >= 500) {
            $(this).parents('.form-group').find('.length-text').text('500/500');
        }
    });
});


$('.sort-services').click(function () {
    $('#menu-services li a').removeClass('active');
    $(this).addClass('active');
    var servicesId = $(this).attr('services-id');
    $.ajax({
        url: '/apiGetServices',
        type: "POST",
        dataType: "json",
        data: {
            "servicesId": servicesId
        },
        success: function (data) {
            // $('.image-services').css('background-image', 'url(' + encodeURI(data.image) + ')');
            $('.image-services-url').attr('src', encodeURI(data.image));
            if (!data.description) {
                $('.text-services').html("");
                $('.price').html("");
            } else {
                $('.price').text('$' + data.price);
                $('.text-services').html(data.description);
            }
        }
    });
});



$(document).on("click", "#accordion .sort-services", function (event) {
    event.preventDefault();
    var id = $(this).attr('id');
    var numElement = parseInt(id.replace("services_", ""))+1;
    $('body,html').animate({scrollTop: 70*numElement}, 1500);
});

$(document).on("click", "#ignismyModal .close", function () {
    $('#ignismyModal').remove();
    // document.location.replace('/contacts');
})

$(document).on('click', '.feedback .rating > label', function () {
    $(this).parents('.rating').find('label').removeClass('active');
    $(this).addClass('active')
});

$(document).on('click', '.btn-send-reviews', function () {
    var id = '';
    var raiting = [];
    var sum = 0;
    var i = 0;
    $('.rating').each(function () {
        id = $(this).find('label.active').attr('for');
        if (id) {
            sum = sum + parseInt($('#' + id).attr('id-stars'));
            raiting.push([
                parseInt($('#' + id).attr('id-stars')),
                parseInt($('#' + id).attr('categoty'))
            ]);
            i++
        }
    });
    if ($('#review-email').val() || $('#review-phone').val()) {
        var email = '';
        var phone = '';
        var text = '';
        if ($('#review-email').val()) {
            email = $('#review-email').val();
        }
        if ($('#review-phone').val()) {
            phone = $('#review-phone').val();
        }
        if ($('#review-text').val()) {
            text = $('#review-text').val();
        }
        var message = '<section class="review-view"><span class="raiting-all">' + parseFloat(sum / i).toFixed(1) + '</span>\n' +
            '<span class="phone">' + phone + '</span><span class="email">' + email + '</span>\n' +
            '<div class="review-text">' + text + '</div></section>'
        $('#review-phone').val('')
        $('#review-email').val('')
        $('#review-text').val('')

        $.ajax({
            url: '/reviewSave',
            type: "POST",
            dataType: "json",
            data: {
                "raiting": raiting,
                "email": email,
                "phone": phone,
                "text": text
            },
            success: function (data) {
                $('#listReviews').prepend(message);
                var success = '  <div class="row"><div class="modal fade show" id="ignismyModal" role="dialog" style="display: block;">\n' +
                    '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">\n' +
                    '<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>\n' +
                    '</div><div class="modal-body"><div class="thank-you-pop">\n' +
                    '<img src="/images/Green-Round-Tick.png" alt="">\n' +
                    '<h1>Thank You!</h1><p>For your time</p></div></div></div></div></div></div>';
                $('body').prepend(success);
            }
        });
    }
});