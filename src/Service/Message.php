<?php

namespace App\Service;

use App\Entity\BlackList;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Message
{
    private $passwordEncoder;
    private $entityManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function validateEmail($email)
    {
        $list = $this->entityManager->getRepository(BlackList::class)->findAll();
        foreach ($list as $item) {
            preg_match('/' . $item->getPattern() . '/', $email, $parsMail);
            if (!empty($parsMail)) {
                return 'true';
            }
        }
        return false;
    }
}