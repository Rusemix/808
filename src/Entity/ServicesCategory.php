<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServicesCategoryRepository")
 */
class ServicesCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $typeCategory;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $status = true;

    private $typeCatView;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getTypeCategory(): ?string
    {
        return $this->typeCategory;
    }

    public function setTypeCategory(int $typeCategory): self
    {
        $this->typeCategory = $typeCategory;

        return $this;
    }

    public function getTypeCatView()
    {
        switch ($this->typeCategory){
            case 1:
                return 'Handymen';
            case 2:
                return 'Siding & Hardbord';
            case 3:
                return 'Roofing & Gutters';
            case 4:
                return 'Doors & Windows';
            case 5:
                return 'Powerwash';
            case 6:
                return 'Design';
        }
    }

}
