<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServicesRepository")
 * @Vich\Uploadable()
 */
class Services
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="services_upload", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeServices;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : true})
     */
    private $status = true;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $typeCategory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $anchor;

    private $typeCatView;

    private $url;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

    }

    public function getUrl(): ?string
    {
        if ($this->typeServices) {
            $url = '/handymen?services=' . strtolower($this->typeServices);
        } else {
            $url = null;
        }
        return $url;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updateAt = new \DateTime();
        }
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTypeServices()
    {
        return $this->typeServices;
    }

    public function setTypeServices($typeServices)
    {
        $this->typeServices = $typeServices;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getTypeCategory(): ?string
    {
        return $this->typeCategory;
    }

    public function setTypeCategory(int $typeCategory): self
    {
        $this->typeCategory = $typeCategory;

        return $this;
    }

    public function getAnchor()
    {
        return $this->anchor;
    }

    public function setAnchor($anchor): void
    {
        $this->anchor = $anchor;
    }

    public function getTypeCatView()
    {
        switch ($this->typeCategory) {
            case 1:
                return 'Handymen';
            case 2:
                return 'Siding & Hardbord';
            case 3:
                return 'Roofing & Gutters';
            case 4:
                return 'Doors & Windows';
            case 5:
                return 'Powerwash';
            case 6:
                return 'Design';
        }
    }
}
