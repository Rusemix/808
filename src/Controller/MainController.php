<?php

namespace App\Controller;

use App\Entity\Advertising;
use App\Entity\JobOffer;
use App\Entity\MainCarousel;
use App\Entity\Orders;
use App\Entity\Review;
use App\Repository\JobOfferRepository;
use App\Service\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class MainController extends AbstractController
{
    private $translator;
    private $locale;
    private $message;

    function __construct(TranslatorInterface $translator, Message $message)
    {
        $this->translator = $translator;
        $this->locale = $translator->getLocale();
        $this->message = $message;
    }

    public function index()
    {
        $listMainCarusel = $this->getDoctrine()->getRepository(MainCarousel::class)->findAll();
        $listAdvertising = $this->getDoctrine()->getRepository(Advertising::class)->findAll();
        return $this->render('main/index.html.twig', [
            'listMainCarusel' => $listMainCarusel,
            'listAdvertising' => $listAdvertising
        ]);
    }

    public function sendJobOffer()
    {
        $out = ['status' => false];
        $request = [];
        parse_str($_REQUEST['data'], $request);
        if (
            !empty($request['name']) &&
            !empty($request['lastName']) &&
            !empty($request['phoneNumber']) &&
            !empty($request['email']) &&
            !empty($request['socialMediaLink']) &&
            !empty($request['skills']) &&
            !empty($request['driverLicence']) &&
            !empty($request['transport']) &&
            !empty($request['statusInUS']) &&
            !empty($request['languageSpeak']) &&
            !empty($request['week']) &&
            !empty($request['hour']) &&
            !empty($request['year'])
        ) {
            $parsEmail = $this->message->validateEmail($request['email']);
            if (empty($parsEmail)) {
                $jobOfferByEmail = $this->getDoctrine()->getRepository(JobOffer::class)->findOneBy(['email' => $request['email']]);
                $jobOfferByPhone = $this->getDoctrine()->getRepository(JobOffer::class)->findOneBy(['phoneNumber' => $request['phoneNumber']]);
                if (!empty($jobOfferByEmail) || !empty($jobOfferByPhone)) {
                    $out = ['status' => false, 'message' => 'This user already exists'];
                } else {
                    $uploads_dir = __DIR__ . '/../../public/job_image_list';
                    $tmp_name = $_FILES["file"]["tmp_name"];
                    $name = basename($request['name'] . '_' . $request['lastName'] . '_' . $request['phoneNumber'] . '.jpg');
                    $entityManager = $this->getDoctrine()->getManager();
                    $jobOffer = new JobOffer();
                    $jobOffer->setName($request['name']);
                    $jobOffer->setLastName($request['lastName']);
                    $jobOffer->setPhoneNumber($request['phoneNumber']);
                    $jobOffer->setEmail($request['email']);
                    $jobOffer->setSocialMediaLink($request['socialMediaLink']);
                    $jobOffer->setSkills(json_encode($request['skills']));
                    $jobOffer->setDriverLicence($request['driverLicence']);
                    $jobOffer->setTransport($request['transport']);
                    $jobOffer->setStatusInUS($request['statusInUS']);
                    $jobOffer->setLanguageSpeak(json_encode($request['languageSpeak']));
                    $jobOffer->setWeek($request['week']);
                    $jobOffer->setHour($request['hour']);
                    $jobOffer->setYear($request['year']);
                    $jobOffer->setStatus(true);
                    $jobOffer->setSend(true);
                    $jobOffer->setImage($name);
                    $entityManager->persist($jobOffer);
                    $entityManager->flush();
                    move_uploaded_file($tmp_name, "$uploads_dir/$name");
                    $out = ['status' => true];
                }
            }
        }
        return new JsonResponse($out);
    }

    public function job()
    {
        return $this->render('main/job.html.twig', [
        ]);
    }

    public function saveContacts()
    {
        if (!empty($_REQUEST['phone']) || !empty($_REQUEST['email'])) {
            $parsEmail = $this->message->validateEmail($_REQUEST['email']);
            if (empty($parsEmail)) {
                $entityManager = $this->getDoctrine()->getManager();
                $modal = new Orders();
                $modal->setFirstName($_REQUEST['firstName']);
                $modal->setLastName($_REQUEST['lastName']);
                $modal->setPhone($_REQUEST['phone']);
                $modal->setSend(1);
                $modal->setEmai($_REQUEST['email']);
                $modal->setAddress($_REQUEST['address']);
                $modal->setMessage(mb_substr($_REQUEST['message'], 0, 500, 'UTF-8'));
                $modal->setStatus(1);
                $entityManager->persist($modal);
                $entityManager->flush();
                $error = array();
                $extension = array("jpeg", "jpg", "png", "gif");
                $i = 0;
                foreach ($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
                    $file_name = $_FILES["files"]["name"][$key];
                    $file_tmp = $_FILES["files"]["tmp_name"][$key];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $txtGalleryName = 'order_' . $modal->getId();

                    $nameDir = 'order_' . $modal->getId();
                    $dir = __DIR__ . '/../../public/uploads/orders/' . $nameDir;
                    if (!file_exists($dir)) {
                        mkdir($dir, 0754);
                    }
                    if (in_array($ext, $extension)) {
                        if (!file_exists("uploads/orders/" . $txtGalleryName . "/" . $file_name)) {
                            move_uploaded_file($file_tmp = $_FILES["files"]["tmp_name"][$key], "uploads/orders/" . $txtGalleryName . "/" . $file_name);
                        } else {
                            $filename = basename($file_name, $ext);
                            $newFileName = $filename . time() . "." . $ext;
                            move_uploaded_file($file_tmp = $_FILES["files"]["tmp_name"][$key], "uploads/orders/" . $txtGalleryName . "/" . $newFileName);
                        }
                    } else {
                        array_push($error, "$file_name, ");
                    }
                    $i++;
                    if ($i == 9) {
                        break;
                    }
                }
            }
        }
        if (!empty($_REQUEST['prev_url'])) {
            return $this->redirect($_REQUEST['prev_url'], 301);
        }
        return $this->redirect('/contacts?success=true', 301);
    }

    public function contacts()
    {
        $messageSave = false;
        if (!empty($_REQUEST['success']) && $_REQUEST['success'] == true) {
            $messageSave = true;
        }
        return $this->render('main/contacts.html.twig', [
            'messageSave' => $messageSave
        ]);
    }

    public function review()
    {
        $listReviews = $this->getDoctrine()->getRepository(Review::class)->findAll();
        $sumScore = 0;
        foreach ($listReviews as $review) {
            $sumScore += ($review->getQualityOfWork() + $review->getSchedule() +
                    $review->getPrice() + $review->getPersonalQuality() + $review->getCleaning()) / 5;
        }
        return $this->render('main/review.html.twig', [
            'listReviews' => $listReviews,
            'score' => round($sumScore / count($listReviews), 2)
        ]);
    }

    public function saveReview()
    {
        if (!empty($_REQUEST['phone']) || !empty($_REQUEST['email'])) {
            $entityManager = $this->getDoctrine()->getManager();
            $modal = new Review();
            foreach ($_REQUEST['raiting'] as $raiting) {
                if ($raiting[1] == 1) {
                    $modal->setQualityOfWork($raiting[0]);
                } else {
                    $modal->setQualityOfWork(5);
                }
                if ($raiting[1] == 2) {
                    $modal->setSchedule($raiting[0]);
                } else {
                    $modal->setSchedule(5);
                }
                if ($raiting[1] == 3) {
                    $modal->setPrice($raiting[0]);
                } else {
                    $modal->setPrice(5);
                }
                if ($raiting[1] == 4) {
                    $modal->setPersonalQuality($raiting[0]);
                } else {
                    $modal->setPersonalQuality(5);
                }
                if ($raiting[1] == 5) {
                    $modal->setCleaning($raiting[0]);
                } else {
                    $modal->setCleaning(5);
                }
            }
            if ($_REQUEST['email']) {
                $modal->setEmail($_REQUEST['email']);
            }
            if ($_REQUEST['phone']) {
                $modal->setPhone($_REQUEST['phone']);
            }
            if ($_REQUEST['text']) {
                $modal->setDescription($_REQUEST['text']);
            }
            $modal->setStatus(1);
            $entityManager->persist($modal);
            $entityManager->flush();
            return new JsonResponse(['success' => true]);
        }
        return new JsonResponse(['success' => false]);
    }

    public function services()
    {
        return $this->render('main/services.html.twig', []);
    }

    public function getNewOrders()
    {
        $orders = $this->getDoctrine()->getRepository(Orders::class)->findBy(['status' => true]);
        return new JsonResponse(['count' => count($orders)]);
    }
}
