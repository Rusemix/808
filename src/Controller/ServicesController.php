<?php

namespace App\Controller;

use App\Entity\Services;
use App\Entity\ServicesCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonRespons;

class ServicesController extends AbstractController
{

    public function getCategoryServices()
    {
        $services = $this->getDoctrine()->getRepository(Services::class)->find($_REQUEST['servicesId']);
        if (!empty($services)) {
            $arrData = [
                'image' => '/uploads/services/' . $services->getImage(),
                'description' => $services->getDescription(),
                'price' => $services->getPrice(),
            ];
        } else {
            $arrData = [];
        }
        return new JsonResponse($arrData);
    }

    public function handymen()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 1, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Handymen'
        ]);
    }

    public function sidingHardbord()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 2, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Siding & Hardbord'
        ]);
    }

    public function roofingGutters()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 3, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Roofing & Gutters'
        ]);
    }

    public function doorsWindows()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 4, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Doors & Windows'
        ]);
    }

    public function powerwash()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 5, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Powerwash'
        ]);
    }

    public function design()
    {
        $category = [];
        $services = $this->getDoctrine()->getRepository(Services::class)
            ->findBy(['typeCategory' => 6, 'status' => true]);
        foreach ($services as $service) {
            $category[] = [
                'id' => $service->getId(),
                'name' => $service->getTypeServices(),
                'anchor' => $service->getAnchor()
            ];
        }
        if (!empty($services[0]) || !empty($_REQUEST['services'])) {
            if (!empty($_REQUEST['services'])) {
                $servicesCategoryInf = $this->getDoctrine()->getRepository(Services::class)
                    ->findOneBy(['typeServices' => trim($_REQUEST['services'])]);
                $idCategory = $servicesCategoryInf->getId();
            } elseif (!empty($services[0])) {
                $idCategory = $services[0]->getId();
            }
            $services = $this->getDoctrine()->getRepository(Services::class)
                ->findOneBy(['id' => $idCategory]);
        }
        return $this->render('main/services.html.twig', [
            'listCategory' => !empty($category) ? $category : null,
            'services' => !empty($services) ? $services : null,
            'title' => 'Design'
        ]);
    }
}
