<?php

namespace App\Controller\Admin;

use App\Entity\JobOffer;
use App\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class JobOfferController extends AbstractController
{
    public function offerList()
    {
        return $this->render('admin/page/offer.html.twig', []);
    }

    public function removeOffer(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ids = explode(',', $request->request->get('ids'));
        foreach ($ids as $id) {
            $repository = $this->getDoctrine()->getRepository(JobOffer::class)->find($id);
            $entityManager->remove($repository);
        }
        $entityManager->flush();
        $response = new JsonResponse([]);
        return $response;
    }

    public function update(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        if ($request->request->get('status') == 'true') {
            $status = true;
        } else {
            $status = false;
        }
        $repository = $this->getDoctrine()->getRepository(JobOffer::class)->find($id);
        $repository->setStatus($status);
        $entityManager->persist($repository);
        $entityManager->flush();
        $response = new JsonResponse([]);
        return $response;
    }

    public function getOffers(Request $request)
    {
        $offersCount = $this->getDoctrine()->getRepository(JobOffer::class)->findAll();
        $offers = $this->getDoctrine()->getRepository(JobOffer::class)->findBy([], ['id' => 'DESC'], $request->query->get('limit'), $request->query->get('offset'));
        $out = [];
        $out['total'] = count($offersCount);
        $out['totalNotFiltered'] = count($offersCount);
        foreach ($offers as $offer) {
            $out['rows'][] = [
                'image' => ['job_image_list/' . $offer->getImage()],
                'id' => $offer->getId(),
                'name' => $offer->getName(),
                'lastName' => $offer->getLastName(),
                'phone' => $offer->getPhoneNumber(),
                'email' => $offer->getEmail(),
                'socialMediaLink' => $offer->getSocialMediaLink(),
                'skills' => "<br>" . implode("<br>", json_decode($offer->getSkills())),
                'driverLicence' => $offer->getDriverLicence(),
                'transport' => $offer->getTransport(),
                'statusInUS' => $offer->getStatusInUS(),
                'languageSpeak' => $offer->getLanguageSpeak(),
                'week' => $offer->getWeek(),
                'hour' => $offer->getHour(),
                'year' => $offer->getYear(),
                'status' => $offer->getStatus(),
                'createdAt' => $offer->getCreatedAt()->format('Y-m-d'),
                'type' => 'job',
            ];
        }
        $response = new JsonResponse($out);
        return $response;
    }
}