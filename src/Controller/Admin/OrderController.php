<?php

namespace App\Controller\Admin;

use App\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends AbstractController
{
    public function orderList()
    {
        $orders = $this->getDoctrine()->getRepository(Orders::class)->findAll();
        return $this->render('admin/page/order.html.twig', [
            'orders' => $orders
        ]);
    }

    public function removeOrder(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ids = explode(',', $request->request->get('ids'));
        foreach ($ids as $id) {
            $repository = $this->getDoctrine()->getRepository(Orders::class)->find($id);
            $entityManager->remove($repository);
        }
        $entityManager->flush();
        $response = new JsonResponse([]);
        return $response;
    }

    public function update(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        if ($request->request->get('status') == 'true') {
            $status = true;
        } else {
            $status = false;
        }
        $repository = $this->getDoctrine()->getRepository(Orders::class)->find($id);
        $repository->setStatus($status);
        $entityManager->persist($repository);
        $entityManager->flush();
        $response = new JsonResponse([]);
        return $response;
    }

    public function getOrders(Request $request)
    {

        $ordersCount = $this->getDoctrine()->getRepository(Orders::class)->findAll();
        $orders = $this->getDoctrine()->getRepository(Orders::class)->findBy([], ['id' => 'DESC'], $request->query->get('limit'), $request->query->get('offset'));

        $out = [];
        $out['total'] = count($ordersCount);
        $out['totalNotFiltered'] = count($ordersCount);
        foreach ($orders as $order) {
            $images = [];
            if (is_dir('uploads/orders/order_' . $order->getId())) {
                foreach (scandir('uploads/orders/order_' . $order->getId()) as $file) {
                    $file = pathinfo('uploads/orders/order_' . $order->getId() . '/' . $file);
                    if ($file['extension'] == 'jpg' || $file['extension'] == 'png' || $file['extension'] == 'jpeg') {
                        $images[] = $file['dirname'] . '/' . $file['basename'];
                    }
                }
            }
            $out['rows'][] = [
                'id' => $order->getId(),
                'firstName' => $order->getFirstName(),
                'LastName' => $order->getLastName(),
                'phone' => $order->getPhone(),
                'email' => $order->getEmai(),
                'address' => $order->getAddress(),
                'message' => $order->getMessage(),
                'status' => $order->getStatus(),
                'image' => $images,
                'type' => 'order',
            ];
        }
        $response = new JsonResponse($out);
        return $response;
    }
}