<?php

namespace App\Controller\Admin;

use App\Entity\BlackList;
use App\Service\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BlackListController extends AbstractController
{
    public function index()
    {
        $blackList = $this->getDoctrine()->getRepository(BlackList::class)->findAll();
        return $this->render('admin/page/black_list.html.twig', ['blackList' => $blackList]);
    }

    public function save(Request $request)
    {
        $out = [];
        $entityManager = $this->getDoctrine()->getManager();
        if (!empty($request->request->get('type'))) {
            if ($request->request->get('type') == 'delete') {
                $repository = $this->getDoctrine()->getRepository(BlackList::class)->find($request->request->get('id'));
                $entityManager->remove($repository);
                $entityManager->flush();
            }
        } else {
            if (!empty($request->request->get('id'))) {
                $blackList = $this->getDoctrine()->getRepository(BlackList::class)->find($request->request->get('id'));
            } else {
                $blackList = new BlackList();
            }
            $blackList->setPattern($request->request->get('pattern'));
            $blackList->setStatus(1);
            $entityManager->persist($blackList);
            $entityManager->flush();
            $out = [
                'id' => $blackList->getId(),
                'pattern' => $blackList->getPattern(),
                'status' => $blackList->getStatus(),
                'created_at' => $blackList->getCreatedAt()->format('Y-m-d')
            ];

        }

        $response = new JsonResponse($out);
        return $response;
    }
}