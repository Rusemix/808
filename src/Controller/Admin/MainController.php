<?php

namespace App\Controller\Admin;

use App\Entity\JobOffer;
use App\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    public function menu()
    {
        $orders = $this->getDoctrine()->getRepository(Orders::class)->findBy(['status'=>1]);
        $jobOffer = $this->getDoctrine()->getRepository(JobOffer::class)->findBy(['status'=>1]);
        return $this->render('admin_menu.html.twig', [
            'countOrders' => count($orders),
            'countJobOffer' => count($jobOffer)
        ]);
    }

}