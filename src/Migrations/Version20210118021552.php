<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118021552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE job_offer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, social_media_link VARCHAR(255) NOT NULL, skills LONGTEXT NOT NULL, driver_licence VARCHAR(255) NOT NULL, transport VARCHAR(255) NOT NULL, status_in_us VARCHAR(255) NOT NULL, language_speak VARCHAR(255) NOT NULL, week VARCHAR(255) NOT NULL, hour VARCHAR(255) NOT NULL, year VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_288A3A4E6B01BC5B (phone_number), UNIQUE INDEX UNIQ_288A3A4EE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE job_offer');
    }
}
