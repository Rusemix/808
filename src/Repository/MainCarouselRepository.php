<?php

namespace App\Repository;

use App\Entity\MainCarousel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MainCarousel|null find($id, $lockMode = null, $lockVersion = null)
 * @method MainCarousel|null findOneBy(array $criteria, array $orderBy = null)
 * @method MainCarousel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MainCarouselRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MainCarousel::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), ['weight' => 'DESC']);
    }
}
